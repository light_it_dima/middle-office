'use strict';

$(function () {
    let $ppc = $('.progress-pie-chart'), 
        percent = parseInt($ppc.data('percent')), 
        deg = 360 * percent / 100;

    if ($ppc) {
        if (percent > 50) {
            $ppc.addClass('gt-50');
        }

        $('.ppc-progress-fill').css('transform', 'rotate(' + deg + 'deg)');
    }
});
