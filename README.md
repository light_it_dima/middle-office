# Getting Started

To setup the project, run:
```sh
$ npm install
$ bower install
```

To start development, run:

```sh
$ gulp serve
```

This will fire up a local web server, open http://localhost:9000 in your default browser and watch files for changes, reloading the browser automatically via [LiveReload].


To make a production-ready build of the app, run:

```sh
$ gulp
```

To preview the production-ready build to check if everything is ok:

```sh
$ gulp serve:dist
```

## Tasks

To get the list of available tasks, run:

```sh
$ gulp --tasks
```

## Linting

For linting JavaScript code ESLint was used. You can use an `.eslintrc` file to configure your project linting rules.

### Adding New Assets

#### Sass

Add a new SCSS file to the "styles" directory, then use `@import` statements to import it in `app/styles/main.scss` like this:

```scss
@import "nav";
```

#### JavaScript

You don't have to worry about new Bower components, their JS files will be automatically injected in the `app/index.html`, but you have to add your own JS files manually. For example, let's say you created `app/scripts/nav.js`, defining some special behavior for the navigation. You should then include it in the comment blocks for your _source_ JS files, where `app/scripts/main.js` is located:

```html
<!-- build:js scripts/main.js -->
<script src="scripts/main.js"></script>
<script src="scripts/nav.js"></script>
<!-- endbuild -->
```

Upon build these will be concatenated and compressed into a single file `scripts/main.js`.

The file name in the comment block and the first source aren't related, their name being the same is a pure coincidence. The file name in the comment block specifies how the final optimized file will be called, while the sources should map to your source files.

## Bower

Installing Bower components is usually as easy as:

```sh
$ bower install --save jquery
```

Behind the scenes [wiredep] will automatically inject assets from your Bower components to your HTML/SCSS files as soon as you run `gulp serve` or `gulp`. If `gulp serve` was already running while installing the components, the injection will happen immediately.

However, in some situations you'll have to do some extra work:

### 1. There are images/fonts in the component

These are a bit tricky, as they can't be automatically injected. Ideally you would want to put them in a place where the link would work both in development and in production, like we do with Bootstrap, but that's sometimes not possible. In those cases you would need to do some [gulp-replace][replace] trickery.

### 2. Field values in the component's `bower.json` are incorrect or missing

If there's a problem, it's usually with the `main` field, which wiredep uses to wire up assets. Fortunately you can always [override][override] these fields inside bower.json.

## Notes
During this trial task the following tools and technologies were used:
* SASS
* HTML5
* jQuery
* Javascript was linting with ESLint
* Icons from [Font Awesome](http://fontawesome.io/)
* [Normalize](http://necolas.github.io/normalize.css/) CSS library was used to set initial css stiles at start
* [Gridilydidily](http://philippkuehn.github.io/gridilydidily/) - SASS based flexbox grid
* [Family.scss](http://lukyvj.github.io/family.scss/) - set of 26 smart Sass mixins which help to manage the style of :nth-child’ified elements, in an easy and classy way
* CSS custom properties
* Inspiration for sass/css code style was taken from [Sass guideline](https://sass-guidelin.es/)
* [Yeoman](http://yeoman.io/) - scaffolding tool for the web development
* Gulp task runner
* NPM
* Git
* Markdown was used for this documentation


[gulp]: https://github.com/gulpjs/gulp
[gulp-docs]: https://github.com/gulpjs/gulp/blob/master/docs/README.md
[yo]: https://github.com/yeoman/yo
[Browser-sync]: https://www.browsersync.io/
[plugins]: https://github.com/jackfranklin/gulp-load-plugins
[eslint-config]: http://eslint.org/docs/user-guide/configuring
[`no-undef`]: http://eslint.org/docs/rules/no-undef
[calc]: https://github.com/postcss/postcss-calc
[wiredep]: https://github.com/taptapship/wiredep
[replace]: https://github.com/lazd/gulp-replace
[override]: https://github.com/taptapship/wiredep#bower-overrides
